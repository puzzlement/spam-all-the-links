import datetime, random, sys, itertools
import jinja2

from bookmarks import BookmarkURLIterator, BookmarkDetailsIterator
from twitterlinks import TwitterURLIterator, TwitterDetailsIterator
from linksinfeed import FeedContentsURLIterator, FeedContentsDetailsIterator
from wordpresscomments import WPCommentsURLIterator, WPCommentsDetailIterator
from cleanurls import URLCache, TitleCache
import configconstants as CC

class NoLinksException(Exception): pass

class Link:
    """
    Represents a URL, a good title for it, and a series of descriptions for it
    from multiple submissions
    """

    def __init__(self, url, title, first_description):
        self.url = url
        self.title = ""
        self.descriptions = set()
        self.merge(title, first_description)

    def merge(self, title, description):
        """
        Merge a potential new title and description for this URL with the current object

        Args:
            title (string): potential new title for this URL
            description (string): new description for this URL
        """
        title = title.strip()
        if len(title) > len(self.title):
            self.title = title

        if description is not None:
            description = description.strip()
            if len(description) > 0:
                self.descriptions.add(description)
    
    def get_jinja_data(self):
        """
        Get representation of this URL to be used in a Jinja template

        Returns:
            dict: Jinja representation of this URL
        """
        desc = u''
        if len(self.descriptions) > 0:
            desc += u': %s' % u", ".join(self.descriptions)
        return {'description': desc, 'title': self.title.strip(), 'url': self.url}

class Links:

    """
    Master iterator for the links found in all sources

    Returns Link objects

    Args:
        template_dir (string): directory containing the Jinja template files for output
    """

    def __init__(self, template_dir, no_empty_mail):
        self.links = {}
        self.existing_links = set()
        self.jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))
        self.no_empty_mail = no_empty_mail

    def addLink(self, url, title, description):
        """
        Add a link to this set of links

        If the link has been previously added, add only its description, and the title if this title is longer.

        Args:
            url (string): URL of the link
            title (string): title of the link
            description (string): description of the link (eg summary or subtitle)
        """
        if url not in self.links:
            self.links[url] = Link(url, title, description)
        else:
            self.links[url].merge(title, description)

    def addExistingLink(self, url):
        self.existing_links.add(url)

    def __iter__(self):
        self.result = [link for link in self.links.values() if link.url not in self.existing_links]
        # shuffle the links so as not to privilege a particular source
        random.shuffle(self.result)
        return self

    def __next__(self):
        try:
            return self.result.pop()
        except IndexError:
            raise StopIteration

    def renderTemplate(self, templatefile):
        template = self.jenv.get_template(templatefile)
        links = [link.get_jinja_data() for link in self]
        if self.no_empty_mail and len(links) == 0:
            raise NoLinksException
        return template.render(links = links)
        
    def renderHTMLPage(self):
        return self.renderTemplate(CC.HTML_TMPL)

    def renderCopyPastaHTML(self):
        return self.renderTemplate(CC.PASTA_TMPL)

    def renderText(self):
        return self.renderTemplate(CC.TEXT_TMPL)

TEMPLATE_DIR="templatedir"

def build_links(config, options):
    url_cache = URLCache()
    title_cache = TitleCache()
    if TEMPLATE_DIR not in config:
        print("Fatal error: %s not found in config file" % TEMPLATE_DIR, file=sys.stderr)
        sys.exit(1)
    links = Links(config[TEMPLATE_DIR], options.no_empty_mail)

    for url, title, description in itertools.chain.from_iterable(source_links(config, options, datetime.datetime.now(), url_cache, title_cache)):
        links.addLink(url, title, description)

    for url in itertools.chain.from_iterable(exclude_links(config, options, url_cache)):
        links.addExistingLink(url)

    return links

def source_links(config, options, now, url_cache, title_cache):

    try:
        sources = config[CC.SOURCES]
    except KeyError:
        print("Fatal error: no sources of links found in JSON config", file=sys.stderr)
        sys.exit(1)

    for config_item, DetailsIterator in [
        (CC.BOOKMARKS, BookmarkDetailsIterator),
        (CC.RSSPOSTS, FeedContentsDetailsIterator),
        (CC.WORDPRESS_POSTS_WITH_COMMENTS, WPCommentsDetailIterator)
    ]:
        if config_item in sources:
            if CC.URLS in sources[config_item]:
                for url in sources[config_item][CC.URLS]:
                    yield DetailsIterator(url, now, options.days, url_cache, title_cache)
            else:
                print("No URLs listed in %s section of JSON config" % config_item, file=sys.stderr)

    if CC.TWITTER in sources:
        if CC.SEARCHES in sources[CC.TWITTER]:
            for search in sources[CC.TWITTER][CC.SEARCHES]:
                yield TwitterDetailsIterator(search, config, now, options.days,
                        url_cache, title_cache)
        else:
            print("No searches listed in %s section of JSON config" % CC.TWITTER, file=sys.stderr)

def exclude_links(config, options, url_cache):

    try:
        exclusions = config[CC.EXCLUSIONS]
    except KeyError:
        # it's OK if there's no sets of links to exclude
        return []

    for config_item, URLIterator in [
        (CC.BOOKMARKS, BookmarkURLIterator),
        (CC.RSSPOSTS, FeedContentsURLIterator),
        (CC.WORDPRESS_POSTS_WITH_COMMENTS, WPCommentsURLIterator)
    ]:
        if config_item in exclusions:
            if CC.URLS in exclusions[config_item]:
                for url in exclusions[config_item][CC.URLS]:
                    yield URLIterator(url, url_cache)
            else:
                print("No URLs listed in %s section of JSON config" % config_item, file=sys.stderr)

    if CC.TWITTER in exclusions:
        if CC.SEARCHES in exclusions[CC.TWITTER]:
            for search in exclusions[CC.TWITTER][CC.SEARCHES]:
                yield TwitterURLIterator(search, config, url_cache)
        else:
            print("No searches listed in %s section of JSON config" % CC.TWITTER, file=sys.stderr)
