import datetime
import feedparser

def is_recent(now, max_days, entry):
    timestamp = datetime.datetime(*entry.updated_parsed[:7])
    daydiff = (now.date() - timestamp.date()).days
    return (daydiff < max_days and daydiff >= 0)

class URLIterator:

    def __init__(self, url_cache):
        self.url_cache = url_cache

    def fetch(self):
        raise NotImplementedError

    def _test(self, feed_item):
        return True

    def __iter__(self):
        self.fetch()
        return self

    def __next__(self):
        raise NotImplementedError

class LinkDetails:

    def __init__(self, now, days, title_cache):
        self.now = now
        self.days = days
        self.title_cache = title_cache

class BookmarkURLIterator(URLIterator):
    def __init__(self, feed_url, url_cache):
        URLIterator.__init__(self, url_cache)
        self.feed_url = feed_url

    def fetch(self):
        self.entries = iter(feedparser.parse(self.feed_url)['entries'])

    def processEntry(self, item):
        canonical_url = self.url_cache[item.link]
        return canonical_url

    def __next__(self):
        while True:
            item = next(self.entries)
            if self._test(item):
                potential = self.processEntry(item)
                if potential is not None:
                    return potential

class BookmarkDetailsIterator(BookmarkURLIterator, LinkDetails):

    def __init__(self, feed_url, now, days, url_cache, title_cache):
        BookmarkURLIterator.__init__(self, feed_url, url_cache)
        LinkDetails.__init__(self, now, days, title_cache)

    def processEntry(self, item):
        canonical_url = BookmarkURLIterator.processEntry(self, item)
        try:
            description = item['summary_detail']['value']
        except KeyError:
            description = None
        return (canonical_url, item.title, description)

    def _test(self, item):
        return is_recent(self.now, self.days, item)
