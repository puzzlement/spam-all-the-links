import datetime, re
import twitter
from bookmarks import URLIterator, LinkDetails
from configconstants import TWITTER_AUTH, KEY, SECRET

URL_SPLIT = re.compile("https?://[^\s]+")
URL_RE = re.compile("(https?://[^\s]+)")

def get_urls(st):
    """http://stackoverflow.com/questions/839994/extracting-a-url-in-python"""
    return [url for url in URL_RE.findall(st)], " ".join(URL_SPLIT.split(st))

class TwitterFailure(Exception): pass

class TwitterURLIterator(URLIterator):

    def __init__(self, search, config, url_cache):
        URLIterator.__init__(self, url_cache)
        self.items = set()
        self.search = search
        self.config = config

    def _get_consumer_secrets(self):
        if TWITTER_AUTH not in self.config:
            raise TwitterFailure("%s not found in configuration file for Twitter authentication" % TWITTER_AUTH)
        if KEY not in self.config[TWITTER_AUTH]:
            raise TwitterFailure("%s not found in configuration file for Twitter authentication" % KEY)
        if SECRET not in self.config[TWITTER_AUTH]:
            raise TwitterFailure("%s not found in configuration file for Twitter authentication" % SECRET)

        return self.config[TWITTER_AUTH][KEY], self.config[TWITTER_AUTH][SECRET]

    def _extend_query(self, query):
        return query

    def _extend_items(self, tweet):
        urls = [item['expanded_url'] for item in tweet['entities']['urls']]
        self.items = self.items.union([self.url_cache[url] for url in urls])

    def fetch(self):
        consumer_key, consumer_secret = self._get_consumer_secrets()
        bearer_token = twitter.oauth2_dance(consumer_key, consumer_secret)

        self.stream = twitter.Twitter(auth=twitter.OAuth2(bearer_token = bearer_token))

        query = [self.search, "filter:links"]
        query = self._extend_query(query)

        self.statuses = self.stream.search.tweets(q=" ".join(query), result_type="recent")['statuses']

    def __next__(self):
        while len(self.items) == 0 and len(self.statuses) > 0:
            tweet = self.statuses.pop()
            self._extend_items(tweet)
        try:
            return self.items.pop()
        except KeyError:
            raise StopIteration

class TwitterDetailsIterator(TwitterURLIterator, LinkDetails):

    def __init__(self, search, config, now, days, url_cache, title_cache):
        TwitterURLIterator.__init__(self, search, config, url_cache)
        LinkDetails.__init__(self, now, days, title_cache)

    def _extend_query(self, query):
        since = datetime.date.today() - datetime.timedelta(days = self.days)
        query.append("since:%s" % since.strftime("%Y-%m-%d"))
        return query

    def _extend_items(self, tweet):
        text = tweet['text']
        _, description = get_urls(text)

        urls = [item['expanded_url'] for item in tweet['entities']['urls']]
        for url in urls:
            canonical_url = self.url_cache[url]
            self.items.add((canonical_url, self.title_cache[canonical_url], description))
