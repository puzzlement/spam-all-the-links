import http, sys, socket
from urllib import request
from urllib.parse import urlencode, urlparse, parse_qs, urlunparse
from bs4 import BeautifulSoup
 
TIMEOUT = 30

socket.setdefaulttimeout(TIMEOUT)

def remove_utm(url):
    # from http://stackoverflow.com/questions/11640353/remove-utm-parameters-from-url-in-python
    parsed = urlparse(url)
    qd = parse_qs(parsed.query, keep_blank_values=True)
    filtered = [(k, v) for k, v in qd.items() if not k.startswith('utm_')]
    newurl = urlunparse([
        parsed.scheme,
        parsed.netloc,
        parsed.path,
        parsed.params,
        urlencode(filtered, doseq=True), # query string
        parsed.fragment
    ])
    return newurl

def custom_urlopen(url):
    req = request.Request(url, headers= {
        'User-Agent': "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11",})
    return request.urlopen(req, timeout=TIMEOUT)

import re

LOCATION_RE = re.compile(r'location.replace.*?"(.*?)"')

def unshort(starturl):
    try:
        # http://stackoverflow.com/a/4902578/1335486
        res = custom_urlopen(starturl)
        return res.geturl()
    except (http.client.BadStatusLine, socket.error, request.HTTPError,
            request.URLError) as e:
        print ("Error trying to unshorten URL %s, leaving "
            "unmodified" % starturl, file=sys.stderr)
        print (str(e), file=sys.stderr)
        return starturl

COMMENT_ANCHOR = '#comments'

def de_comment(url):
    if url[-len(COMMENT_ANCHOR):] == COMMENT_ANCHOR:
        url = url[:-len(COMMENT_ANCHOR)]
    return url

CLEANERS = [unshort, remove_utm, de_comment]

def clean(url):
    chain = [url]
    for cleaner in CLEANERS:
        url = cleaner(url)
        if url not in chain:
            chain.append(url)
    return chain, chain[-1]

def get_title_error(url, e):
    print ("Failed to get title for URL %s" % url, file=sys.stderr)
    print (str(e), file=sys.stderr)
    return "Failed to get title for URL %s" % url

def get_title(url):
    """http://stackoverflow.com/questions/51233/how-can-i-retrieve-the-page-title-of-a-webpage-using-python"""
    try:
        content = request.urlopen(url)
        if 'text/html' in content.headers['content-type'].lower():
            soup = BeautifulSoup(content, "html.parser")
        else:
            return "Failed to get title for URL %s" % url
    except (ValueError, request.HTTPError, request.URLError) as e:
        return get_title_error(url, e)
    
    try:
        return soup.title.string.strip()
    except AttributeError as e:
        return get_title_error(url, e)

class URLCache(dict):

    def __missing__(self, key):
        chain, final = clean(key)
        self[key] = final
        for item in chain:
            self[item] = final
        return final

class TitleCache(dict):

    def __missing__(self, key):
        title = get_title(key)
        self[key] = title
        return title